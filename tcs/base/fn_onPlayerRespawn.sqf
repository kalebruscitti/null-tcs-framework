/**
	Description:
		Event handler for the onPlayerRespawn of the TCS_Respawn respawn template.
*/

["Terminate"] call BIS_fnc_EGSpectator;

if (TCS_var_saveAndRestoreLoadouts) then {
	player setUnitLoadout TCS_var_playerLoadout;
};

[] spawn TCS_fnc_initPlayerRadios;

if (CBA_missionTime > TCS_var_allowVoluntarySpectatorAfter) then {
	[TCS_var_spectatorOptionTimeout] call TCS_fnc_addSpectateAction;
};

if (TCS_var_enableOpticSwap) then {
	[TCS_var_opticSwapArray, TCS_var_opticSwapTimeout] call TCS_fnc_addOpticSwapAction;
};
