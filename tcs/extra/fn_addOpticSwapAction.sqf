//TCS_fnc_addOpticSwapAction
//[TCS_var_opticSwapArray, TCS_var_opticSwapTimeout] call TCS_fnc_addOpticSwapAction;

// Thank you Sherman for originally writing this code.
params ["_opticArray", "_timeOut"];

//get primary weapon
_primaryWeapon = primaryWeapon player;
_primaryWeaponImg = getText(configFile >> "cfgWeapons" >> _primaryWeapon >> "picture");

//create main self interact
_opticSwapMenu = ["OpticSwap", "Optic Swap Menu", _primaryWeaponImg, {}, {true}] call ace_interact_menu_fnc_createAction;
_addActionToObject = [player, 1, ["ACE_SelfActions"], _opticSwapMenu] call ace_interact_menu_fnc_addActionToObject;

//create optic swap subActions within the main self interact
{

_attachment = _x;
_img = getText(configFile >> "cfgWeapons" >> _attachment >> "picture");
_name = getText(configFile >> "cfgWeapons" >> _attachment >> "displayName");

_actionID = _attachment;

_opticOption = [_actionID, _name, _img, {params ["_target", "_player", "_params"]; _player addPrimaryWeaponItem _params;}, {true}, {}, _attachment] call ace_interact_menu_fnc_createAction;
_addActionToObject = [player, 1, ["ACE_SelfActions", "OpticSwap"], _opticOption] call ace_interact_menu_fnc_addActionToObject;

} forEach _opticArray;

//================================
//remove interact from player after x time

[_opticArray, _timeOut] spawn {
	params ["_opticArray", "_timeOut"];
	sleep _timeOut;
	
	//remove optic swap subActions
	{
		[player, 1, ["ACE_SelfActions", "OpticSwap", _x]] call ace_interact_menu_fnc_removeActionFromObject;
	} forEach _opticArray;
	
	//remove main self interact
	[player, 1, ["ACE_SelfActions", "OpticSwap"]] call ace_interact_menu_fnc_removeActionFromObject;
};